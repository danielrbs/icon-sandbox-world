## Introduction

This project is a world using the Sandbox System Builder system on Foundry VTT, containing the basics to play the ICON System by Mastiff Press.

## Usage

Before installing this world, you need to have the latest version of the Sandbox System Builder system installed on your Foundry. After that, install this world by going to Game Worlds > Install World and pasting the following URL:

https://gitlab.com/danielrbs/icon-sandbox-world/-/raw/main/world/world.json

After that, click Install. When the world is installed, open it, and you'll have templates for PC, Foe and Camp sheets. Create your actors using those templates, and DO NOT change anything on the templates themselves or the tabs, panels and dialogs unless you know what you're doing. The world also contains some sample actors in case you need them.
